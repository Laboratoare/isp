/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package horvat.norbert.lab4.ex5;


public class Circle {
    
    public double radius ;
    public String color;
    
    public Circle(){
        color="red";
        radius=1.0;
    }
    
    public Circle ( double radius )
{       
        
        this.radius=radius;
        color="red";
        
}    
    
    Circle(String color){
        this.color=color;
    }  
    

    public double getRadius() {
        return radius;
    }

    public String getColor() {
        return color;
    }
    
   public double getArea()  {
      return radius * Math.PI;
  
   }
    
    
    
}
