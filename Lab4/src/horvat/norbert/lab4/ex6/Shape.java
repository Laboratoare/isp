/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package horvat.norbert.lab4.ex6;


public class Shape {
    
    private String  color   = "red";
    private boolean Filled  = true;
    
 
    
    public Shape(String color ,boolean Filled){
        this.color=color;
        this.Filled=true;
        
    
}  
    public Shape(){}

    public String getColor() {
        return color;
  
    }

    public void setColor(String color) {
        this.color = color;
    }
     

    public boolean isFilled() {
        return Filled;
    }

    public void setFilled(boolean Filled) {
        this.Filled = Filled;
    }

    @Override
    public String toString() {
        return "Shape{" + "color=" + color + ", Filled=" + Filled + '}';
    }
    
    
    
    
    
}
