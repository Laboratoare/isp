/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package horvat.norbert.lab4.ex6;

/**
 *
 * @author Mojo
 */
public class TestShapes {

    public static void main(String[] args) {
        Circle      c1 = new Circle();
        Rectangle   r1 = new Rectangle();
        Square      s1 = new Square();
        System.out.println(c1);
        System.out.println("Aria cerc "+c1.getArea()+"\nperimetru cerc "+c1.getPerimeter());
        System.out.println(r1);
        System.out.println("Arie dreptunghi "+r1.getArea()+"\nRectangle perimeter is "+r1.getPerimeter());
        System.out.println(s1);
        System.out.println("Square Area is "+s1.getArea()+"\nSquare perimeter is "+s1.getPerimeter());
        System.out.println();

        Circle      c2 = new Circle(15);
        Rectangle   r2 = new Rectangle(15, 40);
        Square      s2 = new Square(20);
        System.out.println(c2);
        System.out.println("Aria cerc "+c2.getArea()+"\nperimetru cerc "+c2.getPerimeter());
        System.out.println(r2);
        System.out.println("Arie dreptunghi "+r2.getArea()+"\nperimetru dreptunghi "+r2.getPerimeter());
        System.out.println(s2);
        System.out.println("Arie patrat "+s2.getArea()+"\nperimetru patrat "+s2.getPerimeter());
        System.out.println();

        Circle      c3 = new Circle(15, "green", false);
        Rectangle   r3 = new Rectangle(15, 40, "green", false);
        Square      s3 = new Square(20, "green", false);
        System.out.println(c3);
        System.out.println("Aria cerc "+c3.getArea()+"\nperimetru cerc "+c3.getPerimeter());
        System.out.println(r3);
        System.out.println("Arie dreptunghi "+r3.getArea()+"\nnperimetru dreptunghi "+r3.getPerimeter());
        System.out.println(s3);
        System.out.println("Arie patrat "+s3.getArea()+"\nperimetru patrat "+s3.getPerimeter());
        System.out.println();
    }
}
