
package horvat.norbert.lab4.ex6;


public class Rectangle extends Shape{
    public double width,length;
    
    public Rectangle(){
        super();
         width=1.0;
         length=1.0;
    }
    
    public Rectangle(double width,double lenght){
        
        super();
        this.length=length;
        this.width=width;
        
          
    }
    public Rectangle(double width,double length,String color,boolean filled){
     
        this(width,length);
        setColor(color);
        setFilled(filled);
        
       
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }
    
    public double getArea(){
        
        return 2*(width+length);
    }
    
    public double getPerimeter() {
        return 2 * (width+length);
    }

    @Override
    public String toString() {
        return "Rectangle{" + "width=" + width + ", length=" + length + '}';
    }


    
    
    
    
    
    
    
    
    
}
