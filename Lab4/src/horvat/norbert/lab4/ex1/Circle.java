
package horvat.norbert.lab4.ex1;


public class Circle {
    
    private double radius ;
    private String color;
    
    public Circle(){
        color="red";
        radius=1.0;
    }
    
    public Circle ( double radius )
{       
        
        this.radius=radius;
        color="red";
        
}    
    
    Circle(String color){
        this.color=color;
    }  
    

    public double getRadius() {
        return radius;
    }

    public String getColor() {
        return color;
    }
    
    
    
}
